#include "testdemo.h"

#include "../testwindowdll/CTestDlg.h"

#pragma comment(lib,"../bin/x64/Debug/testwindowdll")

testdemo::testdemo(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);
    OnInitDialog();
}

void testdemo::OnInitDialog()
{
}

void testdemo::on_pushButton_test_clicked()
{
    auto _dlg = new CTestDlg(this);
    Q_ASSERT(_dlg);
    _dlg->show();
}
