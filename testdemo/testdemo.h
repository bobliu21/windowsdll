#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_testdemo.h"

class testdemo : public QMainWindow
{
    Q_OBJECT
private Q_SLOTS:
    void on_pushButton_test_clicked();
public:
    testdemo(QWidget *parent = Q_NULLPTR);
protected:
    void OnInitDialog();
private:
    Ui::testdemoClass ui;
};
