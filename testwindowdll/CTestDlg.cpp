#include "CTestDlg.h"
#include "ui_CTestDlg.h"

CTestDlg::CTestDlg(QWidget *parent)
	: QWidget(parent)
{
	ui->setupUi(this);
	setWindowFlags(Qt::Dialog);
}

CTestDlg::~CTestDlg()
{
	delete ui;
}
