#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(TESTWINDOWDLL_LIB)
#  define TESTWINDOWDLL_EXPORT Q_DECL_EXPORT
# else
#  define TESTWINDOWDLL_EXPORT Q_DECL_IMPORT
# endif
#else
# define TESTWINDOWDLL_EXPORT
#endif
