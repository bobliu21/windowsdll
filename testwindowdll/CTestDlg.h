#pragma once

#include <QWidget>

#include "testwindowdll_global.h"

namespace Ui
{
	class CTestDlg;
}

class TESTWINDOWDLL_EXPORT CTestDlg : public QWidget
{
	Q_OBJECT
signals:
	void signal_test();
public:
	CTestDlg(QWidget *parent = Q_NULLPTR);
	~CTestDlg();

private:
	Ui::CTestDlg *ui;
};
